export const state = () => ({
  darkMode: false
})

export const mutations = {
  setDarkMode(state, value) {
    state.darkMode = value;
  }
  // remove(state, {
  //   todo
  // }) {
  //   state.list.splice(state.list.indexOf(todo), 1)
  // },
  // toggle(state, todo) {
  //   todo.done = !todo.done
  // }
}
