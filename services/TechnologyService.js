import axios from '@nuxtjs/axios'

export default class TechnologyService {


  getTypes() {
    return axios.get("/type/");
  }

  getSubtypes() {
    return axios.get("/subtype");
  }

  getTechnologies() {
    return axios.get("/technology");
  }

  test() {
    return this.$axios.get("/user");
  }

}


// module.exports = class TechnologyService {

//   // url = "https://vyperion-zuul-service.herokuapp.com/tech";

//   getTypes() {
//     return axios.get("/type/");
//   }

//   getSubtypes() {
//     return axios.get("/subtype");
//   }

//   getTechnologies() {
//     return axios.get("/technology");
//   }

//   test() {
//     return "hi";
//   }


// }
