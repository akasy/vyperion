export default ({
  app: {
    $axios
  }
}) => {
  $axios.defaults.baseURL = 'https://zuul.vyperion.com';
  $axios.defaults.headers.get['Content-Type'] = 'application/json';
}
